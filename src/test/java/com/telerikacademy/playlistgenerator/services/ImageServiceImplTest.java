package com.telerikacademy.playlistgenerator.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

import static org.mockito.Mockito.*;

public class ImageServiceImplTest {
    @Mock
    RestTemplate restTemplate;
    @Mock
    Random random;
    @InjectMocks
    ImageServiceImpl imageServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetImageURL() throws Exception {
        String a = "{\"totalHits\":450,\"hits\":[{\"largeImageURL\":\"https://pixabay.com/get/e133b90f2ff01c22d2524518b74d4495ea75ebdd1eac104490f7c27aa6edb4b8_1280.jpg\",\"webformatHeight\":425,\"webformatWidth\":640,\"likes\":635,\"imageWidth\":3456,\"id\":868074,\"user_id\":242387,\"views\":353214,\"comments\":73,\"pageURL\":\"https://pixabay.com/photos/audience-concert-music-868074/\",\"imageHeight\":2298,\"webformatURL\":\"https://pixabay.com/get/e133b90f2ff01c22d2524518b74d4495ea75ebdd1eac104490f7c27aa6edb4b8_640.jpg\",\"type\":\"photo\",\"previewHeight\":99,\"tags\":\"audience, concert, music\",\"downloads\":184273,\"user\":\"Free-Photos\",\"favorites\":920,\"imageSize\":2044512,\"previewWidth\":150,\"userImageURL\":\"https://cdn.pixabay.com/user/2014/05/07/00-10-34-2_250x250.jpg\",\"previewURL\":\"https://cdn.pixabay.com/photo/2015/07/30/17/24/audience-868074_150.jpg\"},{\"largeImageURL\":\"https://pixabay.com/get/ee30b70c2af21c22d2524518b74d4495ea75ebdd1eac104490f7c27aa6edb4b8_1280.jpg\",\"webformatHeight\":425,\"webformatWidth\":640,\"likes\":408,\"imageWidth\":4288,\"id\":756326,\"user_id\":663163,\"views\":141402,\"comments\":44,\"pageURL\":\"https://pixabay.com/photos/guitar-classical-guitar-756326/\",\"imageHeight\":2848,\"webformatURL\":\"https://pixabay.com/get/ee30b70c2af21c22d2524518b74d4495ea75ebdd1eac104490f7c27aa6edb4b8_640.jpg\",\"type\":\"photo\",\"previewHeight\":99,\"tags\":\"guitar, classical guitar, acoustic guitar\",\"downloads\":57683,\"user\":\"FirmBee\",\"favorites\":461,\"imageSize\":2196785,\"previewWidth\":150,\"userImageURL\":\"https://cdn.pixabay.com/user/2015/02/18/12-13-37-583_250x250.png\",\"previewURL\":\"https://cdn.pixabay.com/photo/2015/05/07/11/02/guitar-756326_150.jpg\"},{\"largeImageURL\":\"https://pixabay.com/get/eb3cb30a2af3053ed1584d05fb1d4794e07ee0dc11b60c4090f5c67aa4ecb5b9d8_1280.jpg\",\"webformatHeight\":378,\"webformatWidth\":640,\"likes\":220,\"imageWidth\":3500,\"id\":2925274,\"user_id\":2707530,\"views\":120197,\"comments\":45,\"pageURL\":\"https://pixabay.com/photos/guitar-electric-guitar-2925274/\",\"imageHeight\":2068,\"webformatURL\":\"https://pixabay.com/get/eb3cb30a2af3053ed1584d05fb1d4794e07ee0dc11b60c4090f5c67aa4ecb5b9d8_640.jpg\",\"type\":\"photo\",\"previewHeight\":88,\"tags\":\"guitar, electric guitar, stringed instrument\",\"downloads\":80955,\"user\":\"PIRO4D\",\"favorites\":158,\"imageSize\":1294201,\"previewWidth\":150,\"userImageURL\":\"https://cdn.pixabay.com/user/2018/05/14/13-11-12-127_250x250.jpg\",\"previewURL\":\"https://cdn.pixabay.com/photo/2017/11/07/00/18/guitar-2925274_150.jpg\"},{\"largeImageURL\":\"https://pixabay.com/get/e832b2092afd003ed1584d05fb1d4794e07ee0dc11b60c4090f5c67aa4ecb5b9d8_1280.jpg\",\"webformatHeight\":419,\"webformatWidth\":640,\"likes\":170,\"imageWidth\":4936,\"id\":1736291,\"user_id\":3192627,\"views\":61397,\"comments\":34,\"pageURL\":\"https://pixabay.com/photos/e-guitar-instrument-music-1736291/\",\"imageHeight\":3232,\"webformatURL\":\"https://pixabay.com/get/e832b2092afd003ed1584d05fb1d4794e07ee0dc11b60c4090f5c67aa4ecb5b9d8_640.jpg\",\"type\":\"photo\",\"previewHeight\":98,\"tags\":\"e guitar, instrument, music\",\"downloads\":39952,\"user\":\"obBilder\",\"favorites\":138,\"imageSize\":2437198,\"previewWidth\":150,\"userImageURL\":\"https://cdn.pixabay.com/user/2016/08/29/14-17-47-687_250x250.jpg\",\"previewURL\":\"https://cdn.pixabay.com/photo/2016/10/12/23/22/e-guitar-1736291_150.jpg\"},{\"largeImageURL\":\"https://pixabay.com/get/ef33b50b2bf61c22d2524518b74d4495ea75ebdd1eac104490f7c27aa6edb4b8_1280.jpg\",\"webformatHeight\":426,\"webformatWidth\":640,\"likes\":342,\"imageWidth\":5472,\"id\":664432,\"user_id\":123690,\"views\":101396,\"comments\":33,\"pageURL\":\"https://pixabay.com/photos/musician-rockstar-band-music-rock-664432/\",\"imageHeight\":3648,\"webformatURL\":\"https://pixabay.com/get/ef33b50b2bf61c22d2524518b74d4495ea75ebdd1eac104490f7c27aa6edb4b8_640.jpg\",\"type\":\"photo\",\"previewHeight\":99,\"tags\":\"musician, rockstar, band\",\"downloads\":50980,\"user\":\"RyanMcGuire\",\"favorites\":344,\"imageSize\":5462159,\"previewWidth\":150,\"userImageURL\":\"https://cdn.pixabay.com/user/2014/06/04/17-13-09-273_250x250.jpg\",\"previewURL\":\"https://cdn.pixabay.com/photo/2015/03/08/17/25/musician-664432_150.jpg\"},{\"largeImageURL\":\"https://pixabay.com/get/e834b50621f3083ed1584d05fb1d4794e07ee0dc11b60c4090f5c67aa4ecb5b9d8_1280.jpg\",\"webformatHeight\":480,\"webformatWidth\":640,\"likes\":171,\"imageWidth\":3264,\"id\":1149979,\"user_id\":242387,\"views\":81523,\"comments\":10,\"pageURL\":\"https://pixabay.com/photos/concert-music-crowd-dancings-1149979/\",\"imageHeight\":2448,\"webformatURL\":\"https://pixabay.com/get/e834b50621f3083ed1584d05fb1d4794e07ee0dc11b60c4090f5c67aa4ecb5b9d8_640.jpg\",\"type\":\"photo\",\"previewHeight\":112,\"tags\":\"concert, music, crowd\",\"downloads\":42530,\"user\":\"Free-Photos\",\"favorites\":204,\"imageSize\":1252213,\"previewWidth\":150,\"userImageURL\":\"https://cdn.pixabay.com/user/2014/05/07/00-10-34-2_250x250.jpg\",\"previewURL\":\"https://cdn.pixabay.com/photo/2016/01/19/17/56/concert-1149979_150.jpg\"}]}";
        when(restTemplate.getForObject(anyString(),any())).thenReturn(a);
        when(random.nextInt(6)).thenReturn(0);
        String result = imageServiceImpl.getImageURL("genre");
        Assert.assertEquals(result, "https://pixabay.com/get/e133b90f2ff01c22d2524518b74d4495ea75ebdd1eac104490f7c27aa6edb4b8_640.jpg");
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme