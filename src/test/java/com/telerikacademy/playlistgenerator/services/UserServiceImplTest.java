package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.dtos.PlaylistDTO;
import com.telerikacademy.playlistgenerator.dtos.UserDTO;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.models.User;
import com.telerikacademy.playlistgenerator.repositories.UserRepository;
import com.telerikacademy.playlistgenerator.services.base.PlaylistService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class UserServiceImplTest {
    @Mock
    PasswordEncoder encoder;
    @Mock
    UserDetailsManager userDetailsManager;
    @Mock
    UserRepository userRepository;
    @Mock
    PlaylistService playlistService;
    @InjectMocks
    UserServiceImpl userServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAll() throws Exception {
        User user = new User();
        user.setId(1);
        when(userServiceImpl.getAll()).thenReturn(Arrays.asList(user));
        List<User> result = userServiceImpl.getAll();
        Assert.assertEquals(user.getId(), result.get(0).getId());
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void testCreateUser() throws Exception {
        User user = new User();
        user.setId(1);
        user.setEmail("a@a.a");
        user.setUsername("Username");
        user.setFirstName("First");
        user.setLastName("Last");
        user.setPassword("pass");
        user.setPasswordConfirmation("pass");
        when(encoder.encode(anyString())).thenReturn("pass");
        userServiceImpl.createUser(user);
        verify(userDetailsManager, times(1)).createUser(any());
    }

    @Test
    public void testUserExist() throws Exception {
        User user = new User();
        user.setId(1);
        user.setEmail("a@a.a");
        user.setUsername("Username");
        user.setFirstName("First");
        user.setLastName("Last");
        user.setPassword("pass");
        user.setPasswordConfirmation("pass");
        userServiceImpl.userExist(user);
        verify(userDetailsManager, times(1)).userExists(user.getUsername());
    }

    @Test
    public void testGetUserByUsername() throws Exception {
        userServiceImpl.getUserByUsername("username");
        verify(userRepository, times(1)).getUserByUsername("username");
    }

    @Test
    public void testGetUserById() throws Exception {
        userServiceImpl.getUserById(1);
        verify(userRepository, times(1)).getUserById(1);
    }

    @Test
    public void testDeleteById() throws Exception {
        userServiceImpl.deleteById(1);
        verify(playlistService, times(1)).deleteByUserId(1);
        verify(userRepository, times(1)).deleteById(1);
    }

    @Test
    public void testChangeRole() throws Exception {
        User user = new User();
        user.setId(1);
        user.setEmail("a@a.a");
        user.setUsername("Username");
        user.setFirstName("First");
        user.setLastName("Last");
        user.setPassword("pass");
        user.setPasswordConfirmation("pass");
        when(encoder.encode(anyString())).thenReturn("pass");
        userServiceImpl.changeRole(user);
        verify(userDetailsManager, times(1)).updateUser(any());
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void testUpdateUser() throws Exception {
        User user = new User();
        user.setId(1);
        user.setEmail("a@a.a");
        user.setUsername("Username");
        user.setFirstName("First");
        user.setLastName("Last");
        user.setPassword("pass");
        user.setPasswordConfirmation("pass");
        when(encoder.encode(anyString())).thenReturn("pass");
        UserDTO userDTO = new UserDTO();

        userDTO.setId(1);
        userDTO.setEmail("a@a.a");
        userDTO.setUsername("Username2");
        userDTO.setFirstName("First2");
        userDTO.setLastName("Last2");
        userDTO.setPassword("pass");
        userDTO.setPasswordConfirmation("pass");
        when(userRepository.getUserById(userDTO.getId())).thenReturn(user);

        userServiceImpl.updateUser(userDTO);

        verify(userRepository, times(1)).save(user);
        Assert.assertEquals(user.getUsername(), userDTO.getUsername());
}
}
