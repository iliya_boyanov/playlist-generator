package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.dtos.GenreList;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.repositories.GenreRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static org.mockito.Mockito.*;

public class GenreSynchronizationServiceImplTest {
    @Mock
    Logger LOGGER;
    @Mock
    GenreRepository genreRepository;
    @Mock
    RestTemplate restTemplate;
    @InjectMocks
    GenreSynchronizationServiceImpl genreSynchronizationServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSynchronize() throws Exception {
        Genre genre = new Genre();
        Genre genre2 = new Genre();
        genre.setName("genre");
        genre2.setName("genre2");
        genre.setId(1);
        genre2.setId(2);
        GenreList genreList = new GenreList();
        genreList.setGenreList(Arrays.asList(genre));
        when(restTemplate.getForObject("https://api.deezer.com/genre", GenreList.class))
                .thenReturn(genreList);
        when(genreRepository.findAll()).thenReturn(Arrays.asList(genre2));
        genreSynchronizationServiceImpl.synchronize();
        verify(genreRepository,times(1)).save(any());
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme