package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.models.Artist;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.models.PlaylistTrack;
import com.telerikacademy.playlistgenerator.models.Track;
import com.telerikacademy.playlistgenerator.repositories.PlaylistTrackRepository;
import com.telerikacademy.playlistgenerator.repositories.TrackRepository;
import com.telerikacademy.playlistgenerator.services.base.PlaylistService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class PlaylistTrackServiceImplTest {
    @Mock
    PlaylistTrackRepository playlistTrackRepository;
    @Mock
    TrackRepository trackRepository;
    @Mock
    PlaylistService playlistService;
    @InjectMocks
    PlaylistTrackServiceImpl playlistTrackServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreatePlaylistTracks() throws Exception {
        Playlist playlist = new Playlist();
        playlist.setId(1L);
        Track track = new Track();
        track.setId(1);
        Artist artist = new Artist();
        artist.setName("Name");
        track.setArtist(artist);
        track.setTitle("String");
        List<PlaylistTrack> playlistTracks = new ArrayList<>();
        PlaylistTrack pt = new PlaylistTrack();
        pt.setTrackID(track.getId());
        pt.setPlaylistID(playlist.getId());
        playlistTracks.add(pt);

        Assert.assertEquals(playlistTracks.size(), 1);
    }

    @Test
    public void testConvertSecondsToMinutes() throws Exception {
        String result = playlistTrackServiceImpl.convertSecondsToMinutes(3600);
        Assert.assertEquals("01:00:00", result);
    }

    @Test
    public void testGetTracksByPlaylistId() throws Exception {
        Playlist playlist = new Playlist();
        playlist.setId(1L);
        Track track = new Track();
        track.setId(1);
        Artist artist = new Artist();
        artist.setName("Name");
        track.setArtist(artist);
        track.setTitle("String");
        PlaylistTrack pt = new PlaylistTrack();
        pt.setId(1);
        pt.setPlaylistID(playlist.getId());
        pt.setTrackID(1);
        List<PlaylistTrack> tracks = new ArrayList<>();
        tracks.add(pt);

        when(playlistTrackRepository.findAllByPlaylistID(playlist.getId())).thenReturn(tracks);
        when(trackRepository.findById(pt.getTrackID())).thenReturn(Optional.of(track));

        List<Track> result = playlistTrackServiceImpl.getTracksByPlaylistId(playlist.getId());

        Assert.assertEquals(Arrays.asList(track), result);
    }
}
