package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.dtos.Form;
import com.telerikacademy.playlistgenerator.dtos.GenreWithPercent;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.repositories.GenreRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class GenreServiceImplTest {
    @Mock
    GenreRepository repository;
    @InjectMocks
    GenreServiceImpl genreServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetPercentsByGenre() throws Exception {
        Form form = new Form();
        Genre genre = new Genre();
        genre.setName("genre");
        GenreWithPercent genreWithPercent = new GenreWithPercent();
        genreWithPercent.setInitial(1);
        genreWithPercent.setName("genre");
        Optional<Genre> genre1  = Optional.of(genre);
        when(repository.findById(anyInt())).thenReturn(genre1);
        List<GenreWithPercent> genreWithPercents = Arrays.asList(genreWithPercent);
        form.setGenreWithPercents(genreWithPercents);
        LinkedHashMap<Genre, Integer> genreIntegerLinkedHashMap = new LinkedHashMap<>();
        genreIntegerLinkedHashMap.put(genre,100);
        LinkedHashMap<Genre, Integer> result = genreServiceImpl.getPercentsByGenre(form);
        Assert.assertEquals(genreIntegerLinkedHashMap, result);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testGetPercentsByGenreExpectedException() throws Exception {
        Form form = new Form();
        Genre genre = new Genre();

        genre.setName("genre");
        GenreWithPercent genreWithPercent = new GenreWithPercent();
        genreWithPercent.setInitial(1);
        genreWithPercent.setName("genre");
        genreWithPercent.setPercent(110);
        Optional<Genre> genre1  = Optional.of(genre);
        when(repository.findById(anyInt())).thenReturn(genre1);
        List<GenreWithPercent> genreWithPercents = Arrays.asList(genreWithPercent);
        form.setGenreWithPercents(genreWithPercents);
        LinkedHashMap<Genre, Integer> genreIntegerLinkedHashMap = new LinkedHashMap<>();
        genreIntegerLinkedHashMap.put(genre,100);
        LinkedHashMap<Genre, Integer> result = genreServiceImpl.getPercentsByGenre(form);
        Assert.assertEquals(genreIntegerLinkedHashMap, result);
    }
    @Test
    public void testFindAll() throws Exception {
        List<Genre> result = genreServiceImpl.findAll();
        verify(repository,times(1)).findAll();
    }

    @Test
    public void testFindAllByNameContaining() throws Exception {

        List<Genre> result = genreServiceImpl.findAllByNameContaining("name");
        verify(repository,times(1)).findAllByNameContaining("name");
    }

    @Test
    public void testGetGenreById() throws Exception {

        Genre result = genreServiceImpl.getGenreById(1);
        verify(repository,times(1)).getGenreById(1);
    }
}
