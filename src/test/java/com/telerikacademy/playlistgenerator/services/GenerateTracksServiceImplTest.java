package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.models.Album;
import com.telerikacademy.playlistgenerator.models.Artist;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Track;
import com.telerikacademy.playlistgenerator.repositories.TrackRepository;
import com.telerikacademy.playlistgenerator.services.base.PlaylistTackService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.mockito.Mockito.*;

public class GenerateTracksServiceImplTest {
    @Mock
    Logger LOGGER;
    @Mock
    TrackRepository trackRepository;
    @Mock
    PlaylistTackService playlistTrackService;
    @InjectMocks
    GenerateTracksServiceImpl generateTracksServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGenerateTracks() throws Exception {
        Genre genre = new Genre();
        genre.setName("genre");
        Album album = new Album();
        Artist artist = new Artist();
        artist.setName("name");

        Track track = new Track();
        track.setAlbum(new Album());
        track.setArtist(artist);
        track.setGenre(genre);
        track.setDuration(110);
        track.setTitle("title");
        when(trackRepository.getTracksByGenre(any())).thenReturn(Arrays.<Track>asList(track));
        when(trackRepository.getTrackByGenreOrderByRankDesc(any())).thenReturn(Arrays.<Track>asList(track));
        when(playlistTrackService.convertSecondsToMinutes(anyInt())).thenReturn("convertSecondsToMinutesResponse");

        List<Track> result = generateTracksServiceImpl.generateTracks(new HashMap<Genre, Integer>() {{
            put(genre, 100);
        }}, 2.0, Boolean.FALSE, Boolean.FALSE);
        Assert.assertEquals(Arrays.<Track>asList(track), result);
       // verify(LOGGER,times(2)).info(anyString());
    }
}
