package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.dtos.PlaylistDTO;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.models.PlaylistGenre;
import com.telerikacademy.playlistgenerator.models.PlaylistTrack;
import com.telerikacademy.playlistgenerator.repositories.PlaylistGenreRepository;
import com.telerikacademy.playlistgenerator.repositories.PlaylistRepository;
import com.telerikacademy.playlistgenerator.repositories.PlaylistTrackRepository;
import com.telerikacademy.playlistgenerator.services.base.GenreService;
import com.telerikacademy.playlistgenerator.services.base.ImageService;
import com.telerikacademy.playlistgenerator.services.base.PlaylistService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class PlaylistServiceImplTest {
    @Mock
    Logger LOGGER;
    @Mock
    PlaylistRepository playlistRepository;
    @Mock
    PlaylistGenreRepository playlistGenreRepository;
    @Mock
    PlaylistTrackRepository playlistTrackRepository;
    @Mock
    GenreService genreService;
    @Mock
    ImageService imageService;
    @InjectMocks
    PlaylistServiceImpl playlistServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll()  {
        Playlist playlist = new Playlist();
        playlist.setId(1L);
        when(playlistRepository.findAll()).thenReturn(Arrays.asList(playlist));
        List<Playlist> result = playlistServiceImpl.findAll();
        Assert.assertEquals(playlist.getId(), result.get(0).getId());
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void testGetAllOrderedByRank() {
        Playlist playlist = new Playlist();
        when(playlistRepository.findAllByOrderByRankAvgDesc()).thenReturn(Arrays.asList(playlist));

        List<Playlist> result = playlistServiceImpl.getAllOrderedByRank();
        Assert.assertEquals(Arrays.asList(playlist), result);
    }

    @Test
    public void testGetPlaylistById() {
        Playlist playlist = new Playlist();
        when(playlistRepository.getPlaylistById(anyLong())).thenReturn(playlist);

        Playlist result = playlistServiceImpl.getPlaylistById(Long.valueOf(1));
        Assert.assertEquals(playlist, result);
    }

    @Test
    public void testSave() {
        Playlist playlist = new Playlist();
        playlistServiceImpl.save(playlist);
        verify(playlistRepository, times(1)).save(playlist);
    }

    @Test
    public void testCreate()  {
        when(imageService.getImageURL(anyString())).thenReturn("getImageURLResponse");
        String name = "playlist";
        int userId = 1;
        Genre genre = new Genre();
        LinkedHashMap<Genre, Integer> sortedGenresByPercent = new LinkedHashMap<>();
        sortedGenresByPercent.put(genre,1);
        Playlist result = playlistServiceImpl.create(name, userId, sortedGenresByPercent);
        Assert.assertEquals(name, result.getTitle() );
    }

    @Test
    public void testEditTitle() throws Exception {
        Playlist playlist = new Playlist();
        playlist.setTitle("abc");
        playlistServiceImpl.editTitle(playlist, "name");
        Assert.assertEquals(playlist.getTitle(),"name");
    }
    @Test(expected = IllegalArgumentException.class)
    public void testEditTitleExpectException() throws Exception {
        Playlist playlist = new Playlist();
        playlist.setTitle("abc");
        playlistServiceImpl.editTitle(playlist, "");
    }

    @Test
    public void testEditGenres() throws Exception {
        Playlist playlist = new Playlist();
        when(playlistGenreRepository.findAllByPlaylistID(anyLong())).thenReturn(Arrays.<PlaylistGenre>asList(new PlaylistGenre()));

        playlistServiceImpl.editGenres(new Playlist(), Arrays.<Genre>asList(new Genre()));
        verify(playlistGenreRepository, times(1)).deleteAll(any());
        verify(playlistGenreRepository, times(1)).save(any());

    }
    @Test(expected = IllegalArgumentException.class)
    public void testEditGenresExpectException() throws Exception {
        Playlist playlist = new Playlist();
        when(playlistGenreRepository.findAllByPlaylistID(anyLong())).thenReturn(Arrays.<PlaylistGenre>asList(new PlaylistGenre()));

        playlistServiceImpl.editGenres(new Playlist(), null);

    }
    @Test
    public void testGetPlaylistGenresByPlaylistId() throws Exception {
        Genre genre = new Genre();
        genre.setId(1);
        genre.setName("name");
        Playlist playlist = new Playlist();
        playlist.setId(1L);
        PlaylistGenre playlistGenre = new PlaylistGenre();
        playlistGenre.setGenreID(1);
        playlistGenre.setPlaylistID(1L);
        when(playlistRepository.getPlaylistById(anyLong())).thenReturn(playlist);
        when(playlistGenreRepository.findAllByPlaylistID(anyLong())).thenReturn(Arrays.<PlaylistGenre>asList(playlistGenre));
        when(genreService.getGenreById(anyInt())).thenReturn(genre);



        List<Genre> result = playlistServiceImpl.getPlaylistGenresByPlaylistId(Long.valueOf(1));
        Assert.assertEquals(Arrays.<Genre>asList(genre), result);
    }

    @Test
    public void testSearchByTitle() throws Exception {
        Playlist playlist = new Playlist();
        when(playlistRepository.findAllByTitleContainingOrderByRankAvgDesc(anyString())).thenReturn(Arrays.<Playlist>asList(playlist));

        List<Playlist> result = playlistServiceImpl.searchByTitle("title");
        Assert.assertEquals(Arrays.<Playlist>asList(playlist), result);
    }

    @Test
    public void testSearchByGenre() throws Exception {
        Playlist playlist = new Playlist();
        Genre genre = new Genre();
        PlaylistGenre playlistGenre = new PlaylistGenre();
        genre.setName("name");
        genre.setId(1);
        playlist.setId(1L);
        playlistGenre.setPlaylistID(1L);
        playlistGenre.setGenreID(1);
        when(playlistRepository.findAllByIdInOrderByRankAvgDesc(any())).thenReturn(Arrays.asList(playlist));
        when(playlistGenreRepository.findAllByGenreIDIn(any())).thenReturn(Arrays.<PlaylistGenre>asList(playlistGenre));
        when(genreService.findAllByNameContaining(anyString())).thenReturn(Arrays.<Genre>asList(genre));

        List<Playlist> result = playlistServiceImpl.searchByGenre("genre");
        Assert.assertEquals(Arrays.<Playlist>asList(playlist), result);
    }
    @Test
    public void testSearchByDuration() throws Exception {
        Playlist playlist = new Playlist();
        when(playlistRepository.findAllByDurationBetweenOrderByRankAvgDesc(anyString(), anyString())).thenReturn(Arrays.<Playlist>asList(playlist));

        List<Playlist> result = playlistServiceImpl.searchByDuration(anyString(), anyString());
        Assert.assertEquals(Arrays.<Playlist>asList(playlist), result);
    }
    @Test
    public void testDeleteByUserId() throws Exception {

        playlistServiceImpl.deleteByUserId(Integer.valueOf(0));
        verify(playlistRepository,times(1)).findAllByUserID(anyInt());
    }

    @Test
    public void testGetAllPlaylistsDTO() throws Exception {
        Playlist playlist = new Playlist();
        playlist.setId(1L);
        when(playlistRepository.findAll()).thenReturn((List)Arrays.asList(playlist));
        List<PlaylistDTO> result = playlistServiceImpl.getAllPlaylistsDTO();
        Assert.assertEquals(playlist.getId(), result.get(0).getId());
    }

    @Test
    public void testDeleteById() throws Exception {
        when(playlistGenreRepository.findAllByPlaylistID(anyLong())).thenReturn(Arrays.<PlaylistGenre>asList(new PlaylistGenre()));
        when(playlistTrackRepository.findAllByPlaylistID(anyLong())).thenReturn(Arrays.<PlaylistTrack>asList(new PlaylistTrack()));

        playlistServiceImpl.deleteById(Long.valueOf(1));
    }

    @Test
    public void testUpdatePlaylist() throws Exception {
        when(playlistGenreRepository.findAllByPlaylistID(anyLong())).thenReturn(Arrays.<PlaylistGenre>asList(new PlaylistGenre()));

        playlistServiceImpl.updatePlaylist(new Playlist(), "title", Arrays.<Genre>asList(new Genre()));
    }
}
