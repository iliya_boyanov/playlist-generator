package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.dtos.TrackDTO;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.models.PlaylistTrack;
import com.telerikacademy.playlistgenerator.models.Track;
import com.telerikacademy.playlistgenerator.repositories.PlaylistTrackRepository;
import com.telerikacademy.playlistgenerator.repositories.TrackRepository;
import com.telerikacademy.playlistgenerator.services.base.PlaylistService;
import com.telerikacademy.playlistgenerator.services.base.PlaylistTackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.playlistgenerator.dtos.TrackDTO.fromModel;

@Service
public class PlaylistTrackServiceImpl implements PlaylistTackService {

    private PlaylistTrackRepository playlistTrackRepository;
    private TrackRepository trackRepository;
    private PlaylistService playlistService;

    @Autowired
    public PlaylistTrackServiceImpl(PlaylistTrackRepository playlistTrackRepository, TrackRepository trackRepository, PlaylistService playlistService) {
        this.playlistTrackRepository = playlistTrackRepository;
        this.trackRepository = trackRepository;
        this.playlistService = playlistService;
    }


    @Override
    public void createPlaylistTracks(List<Track> tracks, Playlist playlist) {

        List<PlaylistTrack> playlistTracks = new ArrayList<>();
        Integer rankAvg = 0;
        Integer duration = 0;
        for (Track track : tracks) {
            rankAvg += track.getRank();
            duration += track.getDuration();
            PlaylistTrack currentPlaylistTrack = new PlaylistTrack();
            currentPlaylistTrack.setPlaylistID(playlist.getId());
            currentPlaylistTrack.setTrackID(track.getId());
            playlistTracks.add(currentPlaylistTrack);
        }

        playlist.setRankAvg(rankAvg / tracks.size());
        playlist.setDuration(convertSecondsToMinutes(duration));
        playlistService.save(playlist);
        playlistTrackRepository.saveAll(playlistTracks);
    }

    @SuppressWarnings("Duplicates")
    public String convertSecondsToMinutes(Integer secondsCount) {
        int seconds = secondsCount % 60;
        secondsCount -= seconds;
        long minutesCount = secondsCount / 60;
        long minutes = minutesCount % 60;
        minutesCount -= minutes;
        long hoursCount = minutesCount / 60;
        String hh = "" + hoursCount;
        if (hh.length() == 1) {
            hh = '0' + hh;
        }
        String mm = "" + minutes;
        if (mm.length() == 1) {
            mm = '0' + mm;
        }
        String ss = "" + seconds;
        if (ss.length() == 1) {
            ss = '0' + ss;
        }
        return "" + hh + ":" + mm + ":" + ss;
    }

    public List<Track> getTracksByPlaylistId(Long playlistId) {
        List<PlaylistTrack> playlistTracks = playlistTrackRepository.findAllByPlaylistID(playlistId);
        List<Track> trackIds = new ArrayList<>();
        for (PlaylistTrack playlistTrack : playlistTracks) {
            trackIds.add(trackRepository.findById(playlistTrack.getTrackID()).orElse(null));
        }
        return trackIds;
    }

    @Override
    public List<TrackDTO> toDTO(List<Track> tracks) {
        List<TrackDTO> tracksDTO = new ArrayList<>();
        for (Track track : tracks) {
            tracksDTO.add(fromModel(track));
        }

        return tracksDTO;
    }
}
