package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.components.RestTemplateResponseErrorHandler;
import com.telerikacademy.playlistgenerator.services.base.ImageService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Random;


@Service
public class ImageServiceImpl implements ImageService {
    private RestTemplate restTemplate;
    private Random random;
    @Autowired
    public ImageServiceImpl(RestTemplate restTemplate,Random random){
        this.restTemplate = restTemplate;
        this.random = random;
    }

    public String getImageURL(String genre) {
        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());
        int idxOfSpace = genre.indexOf(" ");
        if(idxOfSpace > 0){
            genre = genre.substring(0,idxOfSpace);
        }
        int idxOfSlash = genre.indexOf("/");
        if(idxOfSlash > 0){
            genre = genre.substring(0,idxOfSlash);
        }

        String url = "https://pixabay.com/api/?key=12392987-f0dda5ed4d2e4fa328e5dbf8d&q="+genre+"+music&image_type=photo";
        String response = restTemplate.getForObject(url, String.class);
//        random.nextInt(6);
        JSONObject jsonObject = new JSONObject(response);
//            int x = (int)(Math.random()*((6)));
        return jsonObject
                .getJSONArray("hits")
                .getJSONObject(random.nextInt(6)).getString("webformatURL");
    }
}
