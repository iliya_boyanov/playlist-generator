package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Track;
import com.telerikacademy.playlistgenerator.repositories.TrackRepository;
import com.telerikacademy.playlistgenerator.services.base.GenerateTracksService;
import com.telerikacademy.playlistgenerator.services.base.PlaylistTackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GenerateTracksServiceImpl implements GenerateTracksService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenerateTracksServiceImpl.class);
    private TrackRepository trackRepository;
    private PlaylistTackService playlistTrackService;

    @Autowired
    public GenerateTracksServiceImpl(TrackRepository trackRepository, PlaylistTackService playlistTrackService) {
        this.trackRepository = trackRepository;
        this.playlistTrackService = playlistTrackService;
    }


    public List<Track> generateTracks(HashMap<Genre, Integer> percentByGenre, Double duration, Boolean useTopRanks, Boolean tracksFromSameArtist) {

        List<Track> playlistTracks = new ArrayList<>();
        Integer durationInSeconds = convertMinutesToSeconds(duration);

        LOGGER.info("Start generating playlist with duration of " + playlistTrackService.convertSecondsToMinutes(durationInSeconds) + " seconds.");
        for (Genre genre : percentByGenre.keySet()) {
            Integer currentGenreDuration = percentByGenre.get(genre);
            Integer currentDuration = getDurationByPercent(currentGenreDuration, durationInSeconds);
            List<Track> currentGenreTracks;

            if (useTopRanks) {
                currentGenreTracks = trackRepository.getTrackByGenreOrderByRankDesc(genre);
            } else {
                currentGenreTracks = trackRepository.getTracksByGenre(genre);
                Collections.shuffle(currentGenreTracks);
            }
            if (currentGenreTracks == null || currentGenreTracks.size() == 0) {
                throw new IllegalArgumentException("Sorry we don't have tracks for genre " + genre.getName());
            }
            Set<Integer> artistIds = new HashSet<>();
            for (Track track : currentGenreTracks) {
                if (track.getDuration() < currentDuration && !artistIds.contains(track.getArtist().getId())) {
                    playlistTracks.add(track);
                } else {
                    continue;
                }
                currentDuration -= track.getDuration();
                if (!tracksFromSameArtist) {
                    artistIds.add(track.getArtist().getId());
                }
            }
            LOGGER.info("Tracks for genre " + genre.getName() + " has been generated.");
        }
        Collections.shuffle(playlistTracks);
        return playlistTracks;
    }

    private Integer getDurationByPercent(Integer percent, Integer duration) {
        double arithmeticPercent = percent / 100.0;
        Integer resultDuration = (int) (duration * arithmeticPercent);
        return resultDuration;
    }

    private Integer convertMinutesToSeconds(Double minutes) {
        Integer durationInSeconds = (int) (minutes * 60);
        return durationInSeconds;
    }
}
