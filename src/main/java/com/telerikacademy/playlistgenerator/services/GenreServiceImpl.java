package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.dtos.Form;
import com.telerikacademy.playlistgenerator.dtos.GenreWithPercent;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.repositories.GenreRepository;
import com.telerikacademy.playlistgenerator.repositories.TrackRepository;
import com.telerikacademy.playlistgenerator.services.base.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class GenreServiceImpl implements GenreService {

    public static final String INVALID_PERCENT_DISTRIBUTION = "Invalid percent distribution";
    private GenreRepository repository;
    private TrackRepository trackRepository;
    @Autowired
    public GenreServiceImpl(GenreRepository repository, TrackRepository trackRepository) {
        this.repository = repository;
        this.trackRepository = trackRepository;
    }


    @Override
    public LinkedHashMap<Genre, Integer> getPercentsByGenre(Form form) {
        List<GenreWithPercent> genreWithPercent = form.getGenreWithPercents();
        HashMap<Genre, Integer> percentByGenre = new HashMap<>();
        for (GenreWithPercent genre : genreWithPercent) {
            if (genre.getInitial() != null) {
                Integer percent = genre.getPercent();
                if (percent == null) {
                    percent = 0;
                }
                percentByGenre.put(repository.findById(genre.getInitial()).orElseThrow(
                        () -> new IllegalArgumentException("Genre with id " + genre.getInitial() + " do not exist")), percent);
            }
        }
        if (percentByGenre.size() == 0) {
            percentByGenre = fillAllGenres();
        }
        LinkedHashMap<Genre, Integer> sortedGenres = sortGenresByPercent(percentByGenre);
        LinkedHashMap<Genre, Integer> genresWithPercentDistribution = distributeRemainingPercents(sortedGenres);

        return genresWithPercentDistribution;
    }

    @Override
    public List<Genre> findAll() {
        return (List<Genre>) repository.findAll();
    }

    @Override
    public List<Genre> findAllByNameContaining(String name) {
        return repository.findAllByNameContaining(name);
    }

    @Override
    public Genre getGenreById(Integer id) {
        return repository.getGenreById(id);
    }

    private LinkedHashMap<Genre, Integer> distributeRemainingPercents(LinkedHashMap<Genre, Integer> genresMap) {
        int percentsRemaining = 100;
        int genresRemaining = genresMap.size();

        for (Genre genre : genresMap.keySet()) {
            int value = genresMap.get(genre);
            if (value > percentsRemaining) {
                throw new IllegalArgumentException(INVALID_PERCENT_DISTRIBUTION);
            }
            if (value > 0) {
                percentsRemaining -= value;
            } else {
                if (genresRemaining > percentsRemaining) {
                    throw new IllegalArgumentException(INVALID_PERCENT_DISTRIBUTION);
                }
                genresMap.put(genre, percentsRemaining / genresRemaining);
                percentsRemaining = percentsRemaining - (percentsRemaining / genresRemaining);
            }
            genresRemaining--;
        }
        if (percentsRemaining != 0) {
            throw new IllegalArgumentException(INVALID_PERCENT_DISTRIBUTION);
        }


        return sortGenresByPercent(genresMap);
    }

    private HashMap<Genre, Integer> fillAllGenres() {
        HashMap<Genre,Integer> map = new HashMap<>();
        List<Genre> genres = (List<Genre>)repository.findAll();
        for (Genre genre : genres) {
            if(trackRepository.countTracksByGenre(genre) > 0){
                map.put(genre,0);
            }
        }
        return map;
    }

    private LinkedHashMap<Genre, Integer> sortGenresByPercent(HashMap<Genre, Integer> percentByGenre) {
        LinkedHashMap<Genre, Integer> sorted = percentByGenre.entrySet().stream()
                .sorted((a, b) -> b.getValue().compareTo(a.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v1, LinkedHashMap::new));
        return sorted;
    }
}

