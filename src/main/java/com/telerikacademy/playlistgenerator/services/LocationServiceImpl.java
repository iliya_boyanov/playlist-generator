package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.components.RestTemplateResponseErrorHandler;
import com.telerikacademy.playlistgenerator.dtos.Form;
import com.telerikacademy.playlistgenerator.services.base.LocationService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class LocationServiceImpl implements LocationService {

    private RestTemplate restTemplate;

    @Autowired
    public LocationServiceImpl(RestTemplateBuilder restTemplateBuilder){
        this.restTemplate = restTemplateBuilder.errorHandler(new RestTemplateResponseErrorHandler()).build();
    }

    public double getTravelDuration(List<Double> from, List<Double> to) {
        String url = "https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix?origins=" + from.get(0).toString() + "," + from.get(1).toString() + "&destinations=" + to.get(0).toString() + "," + to.get(1).toString() + "&travelMode=driving&key=AhkpmnOX3icFulvHmEd1Tv6KKc3wdYq1dlSXOfHnp_ywjE9oi9hpbZitX98kZZzG";

        String response = restTemplate.getForObject(url, String.class);

        JSONObject jsonObject = new JSONObject(response);
        double duration = jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0)
                .getJSONArray("results")
                .getJSONObject(0).getDouble("travelDuration");


        return duration;
    }

    public List<Double> getLocation(String url) {

        List<Double> coordinates;

        String response = restTemplate.getForObject(url, String.class);


        JSONObject jsonObject = new JSONObject(response);
        JSONArray geocodePoints = jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0)
                .getJSONArray("geocodePoints");

        JSONObject geocodePoint = geocodePoints.getJSONObject(0);
        if(geocodePoints.length()>1){
            for(int i=1; i < geocodePoints.length(); i++){
                if(geocodePoints.getJSONObject(i).getJSONArray("usageTypes").toList().contains("Route")){
                    geocodePoint = geocodePoints.getJSONObject(i);
                }
            }
        }

        coordinates=geocodePoint.getJSONArray( "coordinates")
                .toList()
                .stream()
                .map(Object::toString)
                .mapToDouble(Double::parseDouble)
                .boxed()
                .collect(Collectors.toList());

        return coordinates;
    }

    public String convertMapToString(LinkedHashMap<String, String> maps){ // Map<String,String> maps
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("http://dev.virtualearth.net/REST/v1/Locations?");

        int currentLength = stringBuilder.length();

        for (String key : maps.keySet()) {
            if (stringBuilder.length() > 0 && stringBuilder.length() > currentLength) {
                stringBuilder.append("&");
            }
            String value = maps.get(key);
            try {
                stringBuilder.append((key != null ? URLEncoder.encode(key, "UTF-8") : ""));
                stringBuilder.append("=");
                stringBuilder.append(value != null ? URLEncoder.encode(value, "UTF-8").replace("+", " ") : "");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("This method requires UTF-8 encoding support", e);
            }
        }

        String apiKey = "&key=AhkpmnOX3icFulvHmEd1Tv6KKc3wdYq1dlSXOfHnp_ywjE9oi9hpbZitX98kZZzG";
        stringBuilder.append(apiKey);
        return stringBuilder.toString();
    }

    public double getLocationFromForm(Form form) {
        LinkedHashMap locationA = new LinkedHashMap<String, String>(){{
            put("countryRegion", form.getCountryRegionFrom());
            put("adminDistrict", form.getAdminDistrictFrom());
            put("locality", form.getLocalityFrom());
            put("postalCode", form.getPostalCodeFrom());
            put("addressLine", form.getAddressLineFrom());
        }};
        LinkedHashMap locationB = new LinkedHashMap<String, String>(){{
            put("countryRegion", form.getCountryRegionTo());
            put("adminDistrict", form.getAdminDistrictTo());
            put("locality", form.getLocalityTo());
            put("postalCode", form.getPostalCodeTo());
            put("addressLine", form.getAddressLineTo());
        }};
        List<Double> from = null;
        List<Double> to = null;
        try {
            from = getLocation(convertMapToString(locationA));
            to = getLocation(convertMapToString(locationB));
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("Invalid destination address");
        }
        return getTravelDuration(from, to);
    }
}
