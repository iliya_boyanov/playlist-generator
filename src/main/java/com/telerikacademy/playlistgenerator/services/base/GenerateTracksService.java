package com.telerikacademy.playlistgenerator.services.base;

import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Track;

import java.util.HashMap;
import java.util.List;

public interface GenerateTracksService {
    List<Track> generateTracks(HashMap<Genre, Integer> percentByGenre,Double duration, Boolean useTopRanks, Boolean tracksFromSameArtist);
}
