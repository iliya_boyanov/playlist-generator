package com.telerikacademy.playlistgenerator.services.base;

import com.telerikacademy.playlistgenerator.dtos.TrackDTO;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.models.Track;

import java.util.List;

public interface PlaylistTackService {
    void createPlaylistTracks(List<Track> tracks, Playlist playlist);
    List<Track> getTracksByPlaylistId(Long playlistId);
    List<TrackDTO> toDTO (List<Track> users);
    String convertSecondsToMinutes(Integer secondsCount);
}
