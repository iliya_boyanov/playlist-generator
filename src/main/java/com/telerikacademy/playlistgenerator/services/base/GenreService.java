package com.telerikacademy.playlistgenerator.services.base;

import com.telerikacademy.playlistgenerator.dtos.Form;
import com.telerikacademy.playlistgenerator.models.Genre;

import java.util.LinkedHashMap;
import java.util.List;

public interface GenreService {
    LinkedHashMap<Genre, Integer> getPercentsByGenre(Form form);
    List<Genre> findAll();
    List<Genre> findAllByNameContaining(String name);
    Genre getGenreById(Integer id);
}
