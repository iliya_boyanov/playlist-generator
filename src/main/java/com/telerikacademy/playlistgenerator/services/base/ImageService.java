package com.telerikacademy.playlistgenerator.services.base;

public interface ImageService {
    String getImageURL(String genre);
}
