package com.telerikacademy.playlistgenerator.services.base;

import com.telerikacademy.playlistgenerator.dtos.PlaylistDTO;
import com.telerikacademy.playlistgenerator.dtos.UserDTO;
import com.telerikacademy.playlistgenerator.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();
    void createUser(User user);
    boolean userExist(User user);
    User getUserByUsername(String username);
    User getUserById(Integer id);
    void deleteById(Integer id);
    void changeRole(User user);
    List<UserDTO> toDTO (List<User> users);
    List<PlaylistDTO> getPlaylistsOfUser();
    void updateUser(UserDTO userDto);

}
