package com.telerikacademy.playlistgenerator.services.base;

import com.telerikacademy.playlistgenerator.dtos.PlaylistDTO;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Playlist;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

public interface PlaylistService {
    List<Playlist> findAll();
    List<Playlist> getAllOrderedByRank();
    Playlist getPlaylistById(Long id);
    void save(Playlist playlist);
    Playlist create(String playlistName, Integer userID, LinkedHashMap<Genre,Integer> sortedGenresByPercent);
    void editTitle(Playlist playlist, String name);
    void editGenres(Playlist playlist, List<Genre> genres);
    List<Playlist> searchByTitle(String title);
    List<PlaylistDTO> getAllPlaylistsDTO();
    void deleteById(Long id);
    void updatePlaylist(Playlist playlist, String title, List<Genre> genres);
    List<Genre> getPlaylistGenresByPlaylistId(Long id);

    List<Playlist> searchByGenre(String genre);
    List<Playlist> searchByDuration(String from, String to);

    void deleteByUserId(Integer id);

    List<Playlist> findAllOrderedByRank();
}
