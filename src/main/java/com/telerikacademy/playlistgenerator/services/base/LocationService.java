package com.telerikacademy.playlistgenerator.services.base;

import com.telerikacademy.playlistgenerator.dtos.Form;

import java.util.LinkedHashMap;
import java.util.List;

public interface LocationService {
    List<Double> getLocation(String url);
    double getTravelDuration(List<Double> from, List<Double> to);
    String convertMapToString(LinkedHashMap<String, String> maps); //Map<String,String> maps
    double getLocationFromForm(Form form);
}
