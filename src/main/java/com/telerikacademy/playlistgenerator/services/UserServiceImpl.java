package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.dtos.PlaylistDTO;
import com.telerikacademy.playlistgenerator.dtos.UserDTO;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.models.User;
import com.telerikacademy.playlistgenerator.repositories.AuthorityRepository;
import com.telerikacademy.playlistgenerator.repositories.UserRepository;
import com.telerikacademy.playlistgenerator.services.base.PlaylistService;
import com.telerikacademy.playlistgenerator.services.base.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.playlistgenerator.dtos.UserDTO.fromModel;

@Service
public class UserServiceImpl implements UserService {

    private PasswordEncoder encoder;
    private UserDetailsManager userDetailsManager;
    private UserRepository userRepository;
    private PlaylistService playlistService;
    private final AuthorityRepository authorityRepository;

    @Autowired
    public UserServiceImpl(PasswordEncoder encoder, UserDetailsManager userDetailsManager, UserRepository userRepository, PlaylistService playlistService, AuthorityRepository authorityRepository) {
        this.encoder = encoder;
        this.userDetailsManager = userDetailsManager;
        this.userRepository = userRepository;
        this.playlistService = playlistService;
        this.authorityRepository = authorityRepository;
    }

    @Override
    public List<User> getAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public void createUser(User user) {
        org.springframework.security.core.userdetails.User newUser = getUser(user);
        userDetailsManager.createUser(newUser);
        userRepository.save(user);
    }

    private org.springframework.security.core.userdetails.User getUser(User user) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                encoder.encode(user.getPassword()),
                authorities);
    }

    @Override
    public boolean userExist(User user) {
        return userDetailsManager.userExists(user.getUsername());
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public User getUserById(Integer id) {
        return userRepository.getUserById(id);
    }

    @Override
    public void deleteById(Integer id) {
        playlistService.deleteByUserId(id);
        String username = userRepository.findById(id).orElse(new User()).getUsername();
        userRepository.deleteById(id);
        userDetailsManager.deleteUser(username);
    }

    @Override
    public void changeRole(User user) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_ADMIN");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        encoder.encode(user.getPassword()),
                        authorities);
        userDetailsManager.updateUser(newUser);
        userRepository.save(user);
    }

    @Override
    public List<UserDTO> toDTO(List<User> users) {
        List<UserDTO> usersDTO = new ArrayList<>();
        for (User user : users) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


            boolean hasUserRole = authentication.getAuthorities().stream()
                    .anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));

            UserDTO temp = fromModel(user);
            usersDTO.add(fromModel(user));
        }

        return usersDTO;
    }

    @Override
    public List<PlaylistDTO> getPlaylistsOfUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        List<PlaylistDTO> result = new ArrayList<>();

        String username = authentication.getName();
        User user = userRepository.getUserByUsername(username);

        List<Playlist> playlists = playlistService.findAll();
        for (Playlist playlist : playlists) {
            if (playlist.getUserID().equals(user.getId())) {
                PlaylistDTO temp = new PlaylistDTO();
                temp.setGenres(playlistService.getPlaylistGenresByPlaylistId(playlist.getId()));
                result.add(temp.fromModel(playlist));
            }
        }
        return result;
    }

    @Override
    public void updateUser(UserDTO userDto) {
        User user = userRepository.getUserById(userDto.getId());

        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setPasswordConfirmation(userDto.getPasswordConfirmation());
        user.setEmail(userDto.getEmail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        if(user.getPassword().length() == 0){
            throw new IllegalArgumentException("Username/password can't be empty!");
        }
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        encoder.encode(user.getPassword()),
                        authorities);
        userDetailsManager.deleteUser(user.getUsername());
        userDetailsManager.createUser(newUser);
        userRepository.save(user);
    }
}
