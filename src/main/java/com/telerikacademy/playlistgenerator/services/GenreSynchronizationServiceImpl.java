package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.dtos.GenreList;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.repositories.GenreRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class GenreSynchronizationServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(GenreSynchronizationServiceImpl.class);
    private GenreRepository genreRepository;
    private RestTemplate restTemplate;

    @Autowired
    public GenreSynchronizationServiceImpl(GenreRepository genreRepository, RestTemplate restTemplate) {
        this.genreRepository = genreRepository;
        this.restTemplate = restTemplate;
    }

    private List<Genre> getAllGenres(RestTemplate restTemplate) {
        String allGenresUri = "https://api.deezer.com/genre";
        GenreList genreList = restTemplate.getForObject(allGenresUri, GenreList.class);
        return genreList.getGenreList();
    }

    @Scheduled
    public void synchronize() {
        List<Genre> currentGenres = ((List<Genre>) genreRepository.findAll());
        List<Genre> genreList = getAllGenres(restTemplate);
        for (Genre genre : genreList) {
            if (!currentGenres.contains(genre) && !genre.getName().equalsIgnoreCase("all")) {
                genreRepository.save(genre);
                LOGGER.info("Genre with name " + genre.getName() + " has been successfully added.");
            }
        }
        LOGGER.info("Genre synchronization finished successfully.");
    }
}
