package com.telerikacademy.playlistgenerator.services;

import com.telerikacademy.playlistgenerator.dtos.PlaylistDTO;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.models.PlaylistGenre;
import com.telerikacademy.playlistgenerator.repositories.PlaylistGenreRepository;
import com.telerikacademy.playlistgenerator.repositories.PlaylistRepository;
import com.telerikacademy.playlistgenerator.repositories.PlaylistTrackRepository;
import com.telerikacademy.playlistgenerator.services.base.GenreService;
import com.telerikacademy.playlistgenerator.services.base.ImageService;
import com.telerikacademy.playlistgenerator.services.base.PlaylistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlaylistServiceImpl.class);
    private PlaylistRepository playlistRepository;
    private PlaylistGenreRepository playlistGenreRepository;
    private PlaylistTrackRepository playlistTrackRepository;
    private GenreService genreService;
    private ImageService imageService;

    @Autowired
    public PlaylistServiceImpl(PlaylistRepository playlistRepository, PlaylistGenreRepository playlistGenreRepository, GenreService genreService, ImageService imageService, PlaylistTrackRepository playlistTrackRepository) {
        this.playlistRepository = playlistRepository;
        this.playlistGenreRepository = playlistGenreRepository;
        this.playlistTrackRepository = playlistTrackRepository;
        this.genreService = genreService;
        this.imageService = imageService;
    }

    @Override
    public List<Playlist> findAll() {
        return (List<Playlist>) playlistRepository.findAll();

    }

    @Override
    public List<Playlist> getAllOrderedByRank() {
        return playlistRepository.findAllByOrderByRankAvgDesc();
    }

    @Override
    public Playlist getPlaylistById(Long id) {
        return playlistRepository.getPlaylistById(id);
    }

    @Override
    public void save(Playlist playlist) {
        playlistRepository.save(playlist);
    }

    @Override
    public Playlist create(String playlistName, Integer userID, LinkedHashMap<Genre, Integer> sortedGenresByPercent) {
        if (playlistName.length() < 3 || playlistName.length() > 50) {
            throw new IllegalArgumentException("Playlist title should be between 3 and 50 characters");
        }
        Playlist playlist = new Playlist();
        playlist.setTitle(playlistName);
        playlist.setUserID(userID);
        List<Genre> genres = new ArrayList<>(sortedGenresByPercent.keySet());
        playlist.setImageURL(imageService.getImageURL(genres.get(0).getName()));
        playlistRepository.save(playlist);
        savePlaylistGenres(playlist, genres);
        LOGGER.info("Playlist with name " + playlistName + " has been created.");
        return playlist;
    }

    @Override
    public void editTitle(Playlist playlist, String name) {
        if (name == null || name.trim().length() < 3 || name.trim().length() > 50100) {
            throw new IllegalArgumentException("Playlist title should be between 3 and 50 characters");
        }
        playlist.setTitle(name);
        playlistRepository.save(playlist);
    }

    @Override
    public void editGenres(Playlist playlist, List<Genre> genres) {
        if (genres == null || genres.size() == 0) {
            throw new IllegalArgumentException("Playlist should have at least one genre!");
        }
        playlistGenreRepository.deleteAll(playlistGenreRepository.findAllByPlaylistID(playlist.getId()));
        savePlaylistGenres(playlist, genres);

    }

    private void savePlaylistGenres(Playlist playlist, List<Genre> genres) {
        if (genres != null) {
            for (Genre genre : genres) {
                PlaylistGenre currentPlaylistGenre = new PlaylistGenre();
                currentPlaylistGenre.setGenreID(genre.getId());
                currentPlaylistGenre.setPlaylistID(playlist.getId());
                playlistGenreRepository.save(currentPlaylistGenre);
            }
        }
    }

    public List<Genre> getPlaylistGenresByPlaylistId(Long id) {
        List<Genre> genres = new ArrayList<>();
        Playlist playlist = getPlaylistById(id);
        List<PlaylistGenre> playlistGenres = playlistGenreRepository.findAllByPlaylistID(playlist.getId());
        if (playlistGenres != null) {
            for (PlaylistGenre playlistGenre : playlistGenres) {
                genres.add(genreService.getGenreById(playlistGenre.getGenreID()));
            }
        }
        return genres;
    }

    public List<Playlist> searchByTitle(String title) {
        return playlistRepository.findAllByTitleContainingOrderByRankAvgDesc(title);
    }

    @Override
    public List<Playlist> searchByGenre(String genre) {
        List<Genre> genres = genreService.findAllByNameContaining(genre);
        List<Integer> genresIds = genres.stream().mapToInt(Genre::getId).boxed().collect(Collectors.toList());
        List<PlaylistGenre> playlistGenres = playlistGenreRepository.findAllByGenreIDIn(genresIds);
        List<Long> ids = new ArrayList<>();
        for (PlaylistGenre playlistGenre : playlistGenres) {
            ids.add(playlistGenre.getPlaylistID());
        }
        //findAllByIdInOrderByRankAvgDesc
        return playlistRepository.findAllByIdInOrderByRankAvgDesc(ids);
    }

    @Override
    public List<Playlist> searchByDuration(String from, String to) {
        if (from == null && to == null) {
            return (List<Playlist>) playlistRepository.findAll();
        }
        if (from == null || to == null) {
            throw new IllegalArgumentException("Invalid duration parameters");
        }
        if (to.trim().equals("")) {
            to = "99:59:59";
        }
        if (from.trim().equals("")) {
            from = "00:00:00";
        }
        return playlistRepository.findAllByDurationBetweenOrderByRankAvgDesc(from, to);
    }

    @Override
    public void deleteByUserId(Integer id) {
        List<Playlist> playlists = playlistRepository.findAllByUserID(id);
        for (Playlist playlist : playlists) {
            deleteById(playlist.getId());
        }
    }

    @Override
    public List<Playlist> findAllOrderedByRank() {
        return playlistRepository.findAllByOrderByRankAvgDesc();
    }


    @Override
    public List<PlaylistDTO> getAllPlaylistsDTO() {
        List<PlaylistDTO> result = new ArrayList<>();

        List<Playlist> playlists = findAll();
        for (Playlist playlist : playlists) {
            result.add(PlaylistDTO.fromModel(playlist));
        }

        return result;
    }

    @Override
    public void deleteById(Long id) {
        playlistTrackRepository.deleteAll(playlistTrackRepository.findAllByPlaylistID(id));
        playlistGenreRepository.deleteAll(playlistGenreRepository.findAllByPlaylistID(id));
        playlistRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void updatePlaylist(Playlist playlist, String title, List<Genre> genres) {
        editTitle(playlist, title);
        editGenres(playlist, genres);
        LOGGER.info("Playlist with id " + playlist.getId() + " has been edited.");
    }
}
