package com.telerikacademy.playlistgenerator.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomErrorHandler implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request, Model model) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        String errorCode = "";
        String errorStatus = "";
        String errorDescription;

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            errorCode = statusCode.toString();
            errorStatus = HttpStatus.resolve(statusCode).getReasonPhrase();

            if (statusCode == HttpStatus.FORBIDDEN.value()) {
                errorDescription = "These aren't the Droids you're looking for ...";
            } else if (statusCode == HttpStatus.NOT_FOUND.value()) {
                errorDescription = "These aren't the Droids you're looking for ...";
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                errorDescription = "Oh no! Our spaghetti code is not working properly. We will be back soon!";
            } else {
                errorDescription = "Something went wrong!";
            }
        } else {
            errorDescription = "Something went wrong!";
        }

        model.addAttribute("errorCode", errorCode);
        model.addAttribute("errorStatus", errorStatus);
        model.addAttribute("errorDescription", errorDescription);

        return "error-page.html";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}