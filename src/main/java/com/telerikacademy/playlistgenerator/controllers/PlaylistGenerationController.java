package com.telerikacademy.playlistgenerator.controllers;

import com.telerikacademy.playlistgenerator.dtos.Form;
import com.telerikacademy.playlistgenerator.dtos.GenreWithPercent;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.models.Track;
import com.telerikacademy.playlistgenerator.models.User;
import com.telerikacademy.playlistgenerator.services.*;
import com.telerikacademy.playlistgenerator.services.base.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.transaction.Transactional;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class PlaylistGenerationController {
    private GenerateTracksService generateTracksService;
    private GenreService genreService;
    private PlaylistTackService playlistTackService;
    private LocationServiceImpl locationService;
    private PlaylistService playlistService;
    private UserService userService;

    @Autowired
    public PlaylistGenerationController(GenerateTracksServiceImpl generateTracksService, GenreServiceImpl genreService
            , PlaylistTackService playlistTackService, UserService userService
            , LocationServiceImpl locationService, PlaylistService playlistService) {
        this.generateTracksService = generateTracksService;
        this.genreService = genreService;
        this.playlistTackService = playlistTackService;
        this.locationService = locationService;
        this.playlistService = playlistService;
        this.userService = userService;
    }

    @GetMapping("/generation")
    public String showGenerationPage(Model model) {
        Form genreWithPercents = getGenerationForm();
        model.addAttribute("form", genreWithPercents);
        model.addAttribute("view", "fragments/generation");
        return "base-layout";
    }

    @PostMapping("/generation")
    @Transactional
    public String generatePlaylist(@ModelAttribute Form form, Model model, BindingResult bindingResult, Authentication authentication) {
        try {
            double duration = locationService.getLocationFromForm(form);
            if(duration < 1){
                throw new IllegalArgumentException("Invalid destination address");
            }
            LinkedHashMap<Genre, Integer> genresByPercents = genreService.getPercentsByGenre(form);
            List<Track> tracksByGenres = generateTracksService.generateTracks(genresByPercents
                    , duration, form.getUseTopTracks(), form.getAllowSameArtist());
            User user = userService.getUserByUsername(authentication.getName());
            Playlist playlist = playlistService.create(form.getPlaylistTitle(), user.getId(), genresByPercents);
            playlistTackService.createPlaylistTracks(tracksByGenres, playlist);
        } catch (RuntimeException e) {
            Form genreWithPercents = getGenerationForm();
            model.addAttribute("error", e.getMessage());
            model.addAttribute("form", genreWithPercents);
            model.addAttribute("view", "fragments/generation");
            return "base-layout";
        }


        if (bindingResult.hasErrors()) {
            return "redirect:/generation";
        }

        return "redirect:/";
    }

    private Form getGenerationForm() {
        List<GenreWithPercent> genres = (genreService.findAll()).stream().map(GenreWithPercent::fromModel).collect(Collectors.toList());
        Form genreWithPercents = new Form();
        genreWithPercents.setGenreWithPercents(genres);
        return genreWithPercents;
    }

}
