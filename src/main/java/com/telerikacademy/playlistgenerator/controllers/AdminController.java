package com.telerikacademy.playlistgenerator.controllers;

import com.telerikacademy.playlistgenerator.dtos.GenreWithPercent;
import com.telerikacademy.playlistgenerator.dtos.PlaylistDTO;
import com.telerikacademy.playlistgenerator.dtos.PlaylistUpdateForm;
import com.telerikacademy.playlistgenerator.dtos.UserDTO;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.models.User;
import com.telerikacademy.playlistgenerator.services.base.GenreService;
import com.telerikacademy.playlistgenerator.services.base.PlaylistService;
import com.telerikacademy.playlistgenerator.services.base.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AdminController {

    private final UserService userService;
    private final PlaylistService playlistService;
    private final GenreService genreService;

    @Autowired
    public AdminController(UserService userService, PlaylistService playlistService, GenreService genreService) {
        this.userService = userService;
        this.playlistService = playlistService;
        this.genreService = genreService;
    }

    @GetMapping("/admin")
    public String getAllUsers(Model model) {
        model.addAttribute("playlists", playlistService.getAllPlaylistsDTO());
        model.addAttribute("users", userService.toDTO(userService.getAll()));
        model.addAttribute("view", "fragments/admin");
        return "base-layout";
    }

    // CRUD operations over users.

    @PostMapping("/admin/delete-user")
    public String deleteUser(@RequestParam(name = "userId", required = false) Integer userId) {
        userService.deleteById(userId);
        return "redirect:/admin";
    }

    @GetMapping("/admin/update-user/{id}")
    public String updateUser(@PathVariable Integer id, Model model) {
        User user = userService.getUserById(id);
        UserDTO userDTO = UserDTO.fromModel(user);

        model.addAttribute("user", userDTO);
        model.addAttribute("userID", id);
        model.addAttribute("view", "fragments/update-user");
        return "base-layout";
    }

    @PostMapping("/admin/update-user/{id}")
    public String updateUser(@PathVariable Integer id, @ModelAttribute UserDTO user, Model model) {
        user.setId(id);

        try {
            userService.updateUser(user);
        }catch (RuntimeException e){
            model.addAttribute("error", "Username/password can't be empty!");
            model.addAttribute("user", user);
            model.addAttribute("view", "fragments/update-user");
            return "base-layout";
        }
        model.addAttribute("user", user);
        model.addAttribute("view", "fragments/update-user");
        return "redirect:/admin";
    }

    @GetMapping("/admin/create-user")
    public String showCreateUser(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("view", "fragments/create-user");
        return "base-layout";
    }

    @PostMapping("/admin/create-user")
    public String createUser(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password can't be empty!");
            model.addAttribute("user", new User());
            model.addAttribute("view", "fragments/create-user");
            return "base-layout";
        }

        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("error", "Password confirmation doesn't match!");
            model.addAttribute("user", new User());
            model.addAttribute("view", "fragments/create-user");
            return "base-layout";
        }

        if (userService.userExist(user)) {
            model.addAttribute("error", "User with the same username exists!");
            model.addAttribute("user", new User());
            model.addAttribute("view", "fragments/create-user");
            return "base-layout";
        }

        userService.createUser(user);
        return "redirect:/admin";
    }

    // CRUD operations over playlists.

    @PostMapping("/admin/delete-playlist")
    public String deletePlaylist(@RequestParam(name = "playlistId", required = false) Long playlistId) {
        playlistService.deleteById(playlistId);
        return "redirect:/admin";
    }

}

