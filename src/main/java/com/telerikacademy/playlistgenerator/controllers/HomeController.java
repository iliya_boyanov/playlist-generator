package com.telerikacademy.playlistgenerator.controllers;

import com.telerikacademy.playlistgenerator.dtos.PlaylistDTO;
import com.telerikacademy.playlistgenerator.services.base.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class HomeController {
    private PlaylistService playlistService;

    @Autowired
    public HomeController(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }

    @GetMapping("/")
    public String showHomePage(Model model) {
        List<PlaylistDTO> playlists = (playlistService.findAllOrderedByRank().stream().map(PlaylistDTO::fromModel).collect(Collectors.toList()));
        model.addAttribute("playlists", playlists);
        model.addAttribute("view", "fragments/home");
        return "base-layout";
    }

    @GetMapping("/filterByTitle")
    public String filterByTitle(Model model,String title) {
        if (title.equals("")) {
            return "redirect:/";
        }
        List<PlaylistDTO> playlists = (playlistService.searchByTitle(title)).stream().map(PlaylistDTO::fromModel).collect(Collectors.toList());
        model.addAttribute("playlists", playlists);
        model.addAttribute("view", "fragments/home");
        return "base-layout";
    }
    @GetMapping("/filterByGenre")
    public String filterByGenre(Model model,String genre) {
        if (genre.equals("")) {
            return "redirect:/";
        }
        List<PlaylistDTO> playlists = (playlistService.searchByGenre(genre)).stream().map(PlaylistDTO::fromModel).collect(Collectors.toList());
        model.addAttribute("playlists", playlists);
        model.addAttribute("view", "fragments/home");
        return "base-layout";
    }

    @GetMapping("/filterByDuration")
    public String filterByGenre(Model model,String from, String to) {
        List<PlaylistDTO> playlists = (playlistService.searchByDuration(from,to)).stream().map(PlaylistDTO::fromModel).collect(Collectors.toList());
        model.addAttribute("playlists", playlists);
        model.addAttribute("view", "fragments/home");
        return "base-layout";
    }
}
