package com.telerikacademy.playlistgenerator.controllers;

import com.telerikacademy.playlistgenerator.models.User;
import com.telerikacademy.playlistgenerator.services.base.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegister(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("view", "fragments/register");
        return "base-layout";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password can't be empty!");
            model.addAttribute("user", new User());
            model.addAttribute("view", "fragments/register");
            return "base-layout";
        }

        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("error", "Password confirmation doesn't match!");
            model.addAttribute("user", new User());
            model.addAttribute("view", "fragments/register");
            return "base-layout";
        }

        if (userService.userExist(user)) {
            model.addAttribute("error", "User with the same username exists!");
            model.addAttribute("user", new User());
            model.addAttribute("view", "fragments/register");
            return "base-layout";
        }

        userService.createUser(user);
        return "redirect:/";
    }
}
