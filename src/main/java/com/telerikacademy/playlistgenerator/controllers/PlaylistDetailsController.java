package com.telerikacademy.playlistgenerator.controllers;

import com.telerikacademy.playlistgenerator.dtos.PlaylistDTO;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.services.base.PlaylistService;
import com.telerikacademy.playlistgenerator.services.base.PlaylistTackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;


@Controller
public class PlaylistDetailsController {

    private PlaylistService playlistService;
    private PlaylistTackService playlistTrackService;

    @Autowired
    public PlaylistDetailsController(PlaylistService playlistService,
                                     PlaylistTackService playlistTrackService) {
        this.playlistService = playlistService;
        this.playlistTrackService = playlistTrackService;
    }

    @GetMapping("/playlist-details/{id}")
    public String showDetails(@PathVariable Long id, Model model) {
        Playlist playlist = playlistService.getPlaylistById(id);
        if(playlist == null){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Playlist not found!"
            );
        }
        PlaylistDTO playlistDTO = PlaylistDTO.fromModel(playlist);
        model.addAttribute("playlist", playlistDTO);
        model.addAttribute("tracks", playlistTrackService.toDTO(playlistTrackService.getTracksByPlaylistId(id)));
        model.addAttribute("view", "fragments/playlist-details");
        return "base-layout";
    }
}
