package com.telerikacademy.playlistgenerator.controllers;

import com.telerikacademy.playlistgenerator.dtos.GenreWithPercent;
import com.telerikacademy.playlistgenerator.dtos.PlaylistDTO;
import com.telerikacademy.playlistgenerator.dtos.PlaylistUpdateForm;
import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Playlist;
import com.telerikacademy.playlistgenerator.models.User;
import com.telerikacademy.playlistgenerator.services.base.GenreService;
import com.telerikacademy.playlistgenerator.services.base.PlaylistService;
import com.telerikacademy.playlistgenerator.services.base.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Controller
public class UserController {

    private final UserService userService;
    private final PlaylistService playlistService;
    private final GenreService genreService;

    @Autowired
    public UserController(UserService userService, PlaylistService playlistService, GenreService genreService) {
        this.userService = userService;
        this.playlistService = playlistService;
        this.genreService = genreService;
    }

    @GetMapping("/user/details")
    public String userDetails(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            User user = userService.getUserByUsername(auth.getName());

            model.addAttribute("playlists", userService.getPlaylistsOfUser());
            model.addAttribute("user", user);
        }

        model.addAttribute("view", "fragments/user-details");
        return "base-layout";
    }

    @PostMapping("/delete-playlist")
    public String deletePlaylist(@RequestParam(name = "playlistId", required = false) Long playlistId) {
        playlistService.deleteById(playlistId);
        return "redirect:/user/details";
    }

    @GetMapping("/update-playlist/{id}")
    public String updatePlaylist(@PathVariable Long id, Model model) {
        PlaylistUpdateForm form = getPlaylistUpdateForm(id);
        model.addAttribute("form", form);
        model.addAttribute("view", "fragments/update-playlist");
        return "base-layout";
    }

    @PostMapping("/update-playlist/{id}")
    public String updatePlaylist(@PathVariable Long id, @ModelAttribute PlaylistUpdateForm form, Model model, HttpServletRequest request) {
        if (request.isUserInRole("ROLE_USER")
                && userService.getUserByUsername(request.getUserPrincipal().getName()).getId() != playlistService.getPlaylistById(form.getId()).getUserID()) {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "Forbidden!"
            );
        }
        List<Genre> genresArray = new ArrayList<>();
        for (GenreWithPercent genre : form.getGenres()) {
            if (genre.getInitial() != null) {
                genresArray.add(genreService.getGenreById(genre.getInitial()));
            }
        }
        try {
            Playlist playlist = playlistService.getPlaylistById(id);
            playlistService.updatePlaylist(playlist, form.getTitle(), genresArray);
        } catch (RuntimeException e) {
            PlaylistUpdateForm errorForm = getPlaylistUpdateForm(id);
            model.addAttribute("error", e.getMessage());
            model.addAttribute("form", errorForm);
            model.addAttribute("view", "fragments/update-playlist");
            return "base-layout";
        }

        if (request.isUserInRole("ROLE_ADMIN")) {
            return "redirect:/admin";
        }
        return "redirect:/user/details";
    }

    private PlaylistUpdateForm getPlaylistUpdateForm(@PathVariable Long id) {
        Playlist playlist = playlistService.getPlaylistById(id);
        List<Genre> genres = genreService.findAll();
        List<Genre> playlistGenres = playlistService.getPlaylistGenresByPlaylistId(id);
        PlaylistDTO playlistDTO = PlaylistDTO.fromModel(playlist);
        List<GenreWithPercent> genreWithPercents = genres.stream().map(GenreWithPercent::fromModel).collect(Collectors.toList());
        genreWithPercents = GenreWithPercent.fillInitialFromGenreList(playlistGenres, genreWithPercents);
        PlaylistUpdateForm form = new PlaylistUpdateForm();
        form.setId(playlistDTO.getId());
        form.setTitle(playlistDTO.getTitle());
        form.setGenres(genreWithPercents);
        return form;
    }
}
