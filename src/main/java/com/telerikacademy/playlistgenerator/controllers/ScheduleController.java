package com.telerikacademy.playlistgenerator.controllers;

import com.telerikacademy.playlistgenerator.services.GenreSynchronizationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.concurrent.ScheduledFuture;

@Controller
class ScheduleController {
    private static final long FIXED_DELAY_ONE_HOUR = 3600000;
    private TaskScheduler taskScheduler;
    private GenreSynchronizationServiceImpl genreSynchronizationService;

    @Autowired
    public ScheduleController(TaskScheduler taskScheduler, GenreSynchronizationServiceImpl genreSynchronizationService) {
        this.taskScheduler = taskScheduler;
        this.genreSynchronizationService = genreSynchronizationService;
    }


    ScheduledFuture<?> scheduledFuture;

    @RequestMapping("admin/start")
    public String start() {
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
        }
        scheduledFuture = taskScheduler.scheduleAtFixedRate(synchronizeGenre(), FIXED_DELAY_ONE_HOUR);
        return "redirect:/admin";
    }

    private Runnable synchronizeGenre() {
        return () -> {
            genreSynchronizationService.synchronize();
        };
    }

}