package com.telerikacademy.playlistgenerator.repositories;

import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Track;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TrackRepository extends CrudRepository<Track, Integer> {
    List<Track> getTracksByGenre(Genre genre);
    Integer countTracksByGenre(Genre genre);
    List<Track> getTrackByTitle(String title);

    List<Track> getTrackByGenreOrderByRankDesc(Genre genre);

    List<Track> findByIdIn(List<Integer> ids);
}
