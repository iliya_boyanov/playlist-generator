package com.telerikacademy.playlistgenerator.repositories;

import com.telerikacademy.playlistgenerator.models.Playlist;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlaylistRepository extends CrudRepository<Playlist, Long> {
    Playlist getPlaylistById(Long id);
    List<Playlist> findAllByTitleContainingOrderByRankAvgDesc(String title);
    List<Playlist> findAllByDurationBetweenOrderByRankAvgDesc(String from, String to);
    List<Playlist> findAllByUserID(Integer id);
    List<Playlist> findAllByOrderByRankAvgDesc();
    List<Playlist> findAllByIdInOrderByRankAvgDesc(List<Long> ids);
    void deleteById(Long aLong);
}
