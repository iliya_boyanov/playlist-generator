package com.telerikacademy.playlistgenerator.repositories;

import com.telerikacademy.playlistgenerator.models.Artist;
import org.springframework.data.repository.CrudRepository;


public interface ArtistRepository extends CrudRepository<Artist,Integer> {
    Artist getArtistByName(String name);

}
