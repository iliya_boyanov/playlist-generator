package com.telerikacademy.playlistgenerator.repositories;

import com.telerikacademy.playlistgenerator.models.PlaylistGenre;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlaylistGenreRepository extends CrudRepository<PlaylistGenre, Integer> {
    List<PlaylistGenre> findAllByGenreIDIn(List<Integer> ids);
    List<PlaylistGenre> findAllByGenreID(Integer id);
    List<PlaylistGenre> findAllByPlaylistID(Long id);
}
