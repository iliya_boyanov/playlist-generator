package com.telerikacademy.playlistgenerator.repositories;

import com.telerikacademy.playlistgenerator.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User,Integer> {
    User getUserByUsername(String username);
    User getUserById(Integer id);
    void deleteById(Integer id);
}
