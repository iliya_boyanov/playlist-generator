package com.telerikacademy.playlistgenerator.repositories;

import com.telerikacademy.playlistgenerator.models.Genre;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface GenreRepository extends CrudRepository<Genre, Integer> {

    Genre getGenreById(Integer id);
    Optional<Genre> getGenreByName(String name);
    List<Genre> findAllByNameContaining(String name);
}
