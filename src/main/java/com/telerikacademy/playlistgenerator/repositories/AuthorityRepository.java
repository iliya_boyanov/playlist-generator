package com.telerikacademy.playlistgenerator.repositories;

import com.telerikacademy.playlistgenerator.models.Authority;
import org.springframework.data.repository.CrudRepository;

public interface AuthorityRepository  extends CrudRepository<Authority,String> {
    void deleteAllByUsername(String username);
}
