package com.telerikacademy.playlistgenerator.repositories;

import com.telerikacademy.playlistgenerator.models.PlaylistTrack;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlaylistTrackRepository extends CrudRepository<PlaylistTrack, Integer> {
    List<PlaylistTrack> findAllByPlaylistID(Long id);
}
