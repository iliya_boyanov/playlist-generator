package com.telerikacademy.playlistgenerator.repositories;

import com.telerikacademy.playlistgenerator.models.Album;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AlbumRepository extends CrudRepository<Album,Integer> {
    Album getAlbumByTitle(String title);
    //    Album getFirstByIdBetweenAndTitleInOrdOrderByTitleAsc(Integer id, List<String> titles);
}
