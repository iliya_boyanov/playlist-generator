package com.telerikacademy.playlistgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;

import java.util.Random;
import java.util.concurrent.TimeUnit;


@SpringBootApplication
public class PlaylistGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlaylistGeneratorApplication.class, args);
    }

    @Bean
    TaskScheduler threadPoolTaskScheduler() {
        return new ThreadPoolTaskScheduler();
    }


    @Bean
    public PeriodicTrigger periodicFixedDelayTrigger() {
        PeriodicTrigger periodicTrigger = new PeriodicTrigger(5, TimeUnit.SECONDS);
        periodicTrigger.setFixedRate(false);
        periodicTrigger.setInitialDelay(1);
        return periodicTrigger;
    }

    @Bean
    public Random getRandomIntBetween(){
      return new Random();
    }
}
