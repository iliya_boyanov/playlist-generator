package com.telerikacademy.playlistgenerator.dtos;

import com.telerikacademy.playlistgenerator.models.Genre;

import java.util.ArrayList;
import java.util.List;

public class GenreWithPercent {
    Integer id;
    String name;
    Integer percent;
    Integer initial;

    public GenreWithPercent() {
    }



    public Integer getInitial() {
        return initial;
    }

    public void setInitial(Integer initial) {
        this.initial = initial;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    public static GenreWithPercent fromModel(Genre genre) {
        GenreWithPercent genreWithPercent = new GenreWithPercent();
        genreWithPercent.setId(genre.getId());
        genreWithPercent.setName(genre.getName());
        return genreWithPercent;
    }

    public static List<GenreWithPercent> fillInitialFromGenreList(List<Genre> genres, List<GenreWithPercent> genreWithPercents){
        for (Genre genre : genres) {
            for (GenreWithPercent genreWithPercent : genreWithPercents) {
                if(genre.getId() == genreWithPercent.getId()){
                    genreWithPercent.setInitial(genre.getId());
                }
            }
        }
        return genreWithPercents;
    }
}
