package com.telerikacademy.playlistgenerator.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telerikacademy.playlistgenerator.models.Artist;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtistList {

    @JsonProperty("data")
    private List<Artist> artists = new ArrayList<>();

    public ArtistList(){
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }
}
