package com.telerikacademy.playlistgenerator.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telerikacademy.playlistgenerator.models.Genre;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GenreList {

    @JsonProperty("data")
    List<Genre> genreList;

    public GenreList() {
        this.genreList = new ArrayList<>();
    }

    public List<Genre> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<Genre> genreList) {
        this.genreList = genreList;
    }
}
