package com.telerikacademy.playlistgenerator.dtos;

import com.telerikacademy.playlistgenerator.models.*;

public class TrackDTO {
   public Integer id;

    public String title;

    public String link;

    public Integer rank;

    public String preview;

    public  String artist;

    public String album;

    public String genre;

    public String duration;

    public static TrackDTO fromModel(Track track) {
        TrackDTO trackDTO = new TrackDTO();
        trackDTO.id = track.getId();
        trackDTO.title = track.getTitle();
        trackDTO.link = track.getLink();
        trackDTO.rank = track.getRank();
        trackDTO.preview = track.getPreview();
        trackDTO.artist = track.getArtist().getName();
        trackDTO.album = track.getAlbum().getTitle();
        trackDTO.genre = track.getGenre().getName();
        trackDTO.duration = convertSecondsToMinutes(track.getDuration());
        return trackDTO;
    }

    private static String convertSecondsToMinutes(Integer secondsCount) {
        int seconds = secondsCount % 60;
        secondsCount -= seconds;
        long minutesCount = secondsCount / 60;
        long minutes = minutesCount % 60;
        minutesCount -= minutes;
        long hoursCount = minutesCount / 60;
        String hh = "" + hoursCount;
        if (hh.length() == 1) {
            hh = '0' + hh;
        }
        String mm = "" + minutes;
        if (mm.length() == 1) {
            mm = '0' + mm;
        }
        String ss = "" + seconds;
        if (ss.length() == 1) {
            ss = '0' + ss;
        }
        return "" + hh + ":" + mm + ":" + ss;
    }
}
