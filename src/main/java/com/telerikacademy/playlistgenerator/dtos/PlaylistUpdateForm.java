package com.telerikacademy.playlistgenerator.dtos;

import java.util.List;

public class PlaylistUpdateForm {
    public Long id;
    public String title;
    public List<GenreWithPercent> genres;

    public PlaylistUpdateForm() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<GenreWithPercent> getGenres() {
        return genres;
    }

    public void setGenres(List<GenreWithPercent> genres) {
        this.genres = genres;
    }
}
