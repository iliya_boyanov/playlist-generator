package com.telerikacademy.playlistgenerator.dtos;

import java.util.List;

public class Form {

    private List<GenreWithPercent> genreWithPercents;
    private String countryRegionFrom;
    private String adminDistrictFrom;
    private String localityFrom;
    private String postalCodeFrom;
    private String addressLineFrom;
    private String adminDistrictTo;
    private String localityTo;
    private String postalCodeTo;
    private String addressLineTo;
    private String countryRegionTo;
    private String playlistTitle;
    private Boolean useTopTracks;
    private Boolean allowSameArtist;

    public Form() {
    }

    public String getAdminDistrictFrom() {
        return adminDistrictFrom;
    }

    public void setAdminDistrictFrom(String adminDistrictFrom) {
        this.adminDistrictFrom = adminDistrictFrom;
    }

    public String getLocalityFrom() {
        return localityFrom;
    }

    public void setLocalityFrom(String localityFrom) {
        this.localityFrom = localityFrom;
    }

    public String getPostalCodeFrom() {
        return postalCodeFrom;
    }

    public void setPostalCodeFrom(String postalCodeFrom) {
        this.postalCodeFrom = postalCodeFrom;
    }

    public String getAddressLineFrom() {
        return addressLineFrom;
    }

    public void setAddressLineFrom(String addressLineFrom) {
        this.addressLineFrom = addressLineFrom;
    }

    public Boolean getUseTopTracks() {
        return useTopTracks;
    }

    public void setUseTopTracks(Boolean useTopTracks) {
        this.useTopTracks = useTopTracks;
    }

    public Boolean getAllowSameArtist() {
        return allowSameArtist;
    }

    public void setAllowSameArtist(Boolean allowSameArtist) {
        this.allowSameArtist = allowSameArtist;
    }

    public String getPlaylistTitle() {
        return playlistTitle;
    }

    public void setPlaylistTitle(String playlistTitle) {
        this.playlistTitle = playlistTitle;
    }

    public List<GenreWithPercent> getGenreWithPercents() {
        return genreWithPercents;
    }

    public void setGenreWithPercents(List<GenreWithPercent> genreWithPercents) {
        this.genreWithPercents = genreWithPercents;
    }

    public String getCountryRegionFrom() {
        return countryRegionFrom;
    }

    public void setCountryRegionFrom(String countryRegionFrom) {
        this.countryRegionFrom = countryRegionFrom;
    }

    public String getCountryRegionTo() {
        return countryRegionTo;
    }

    public void setCountryRegionTo(String countryRegionTo) {
        this.countryRegionTo = countryRegionTo;
    }
    public String getAdminDistrictTo() {
        return adminDistrictTo;
    }

    public void setAdminDistrictTo(String adminDistrictTo) {
        this.adminDistrictTo = adminDistrictTo;
    }

    public String getLocalityTo() {
        return localityTo;
    }

    public void setLocalityTo(String localityTo) {
        this.localityTo = localityTo;
    }

    public String getPostalCodeTo() {
        return postalCodeTo;
    }

    public void setPostalCodeTo(String postalCodeTo) {
        this.postalCodeTo = postalCodeTo;
    }

    public String getAddressLineTo() {
        return addressLineTo;
    }

    public void setAddressLineTo(String addressLineTo) {
        this.addressLineTo = addressLineTo;
    }


}
