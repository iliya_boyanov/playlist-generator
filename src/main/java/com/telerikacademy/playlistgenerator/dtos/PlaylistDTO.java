package com.telerikacademy.playlistgenerator.dtos;

import com.telerikacademy.playlistgenerator.models.Genre;
import com.telerikacademy.playlistgenerator.models.Playlist;

import java.util.List;


public class PlaylistDTO {

    private Long id;

    private String title;

    private String imageURL;

    private String duration;

    private Integer rank;

    private List<Genre> genres;

    public Long getId() {
        return id;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public static PlaylistDTO fromModel(Playlist playlist) {
        PlaylistDTO playlistDTO = new PlaylistDTO();
        playlistDTO.id = playlist.getId();
        playlistDTO.title = playlist.getTitle();
        playlistDTO.imageURL = playlist.getImageURL();
        if (playlist.getDuration() != null) {
            playlistDTO.duration = playlist.getDuration();
        } else {
            playlistDTO.duration = String.valueOf(playlist.getDuration());
        }
        playlistDTO.rank = playlist.getRankAvg();
        return playlistDTO;
    }

}
