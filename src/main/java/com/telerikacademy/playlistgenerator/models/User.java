package com.telerikacademy.playlistgenerator.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user_details")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @NotNull(message = "Username is required")
    @Size(min = 3, message = "Username should be between 3 and 45 characters")
    @Column(name = "user_name")
    private String username;

    @NotNull(message = "Password is required")
    @Size(min = 4, message = "Password should be more than 3 symbols")
    @Transient
    private String password;

    @Transient
    private String passwordConfirmation;

    @NotNull(message = "Email is required")
    @Size(min = 5, max = 100, message = "Email size should be between 5 and 100 characters")
    @Column(name = "email")
    private String email;

    @NotNull(message = "First name is required")
    @Size(min = 3, max = 45, message = "First name size should be between 3 and 45 characters")
    @Column(name = "first_name")
    private String firstName;

    @Size(min = 3, max = 45, message = "First name size should be between 3 and 45 characters")
    @Column(name = "last_name")
    private String lastName;

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
