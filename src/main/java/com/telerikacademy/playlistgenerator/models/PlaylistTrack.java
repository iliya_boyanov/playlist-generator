package com.telerikacademy.playlistgenerator.models;

import javax.persistence.*;

@Entity
@Table(name = "playlist_track_relations")
public class PlaylistTrack {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "playlist_id")
    private Long playlistID;

    @Column(name = "track_id")
    private Integer trackID;

    public PlaylistTrack() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getPlaylistID() {
        return playlistID;
    }

    public void setPlaylistID(Long playlistID) {
        this.playlistID = playlistID;
    }

    public Integer getTrackID() {
        return trackID;
    }

    public void setTrackID(Integer trackID) {
        this.trackID = trackID;
    }
}
