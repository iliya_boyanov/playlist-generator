package com.telerikacademy.playlistgenerator.models;

import javax.persistence.*;

@Entity
@Table(name = "genre_playlist_relations")
public class PlaylistGenre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "genre_id")
    private Integer genreID;

    @Column(name = "playlist_id")
    private Long playlistID;

    public PlaylistGenre() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGenreID() {
        return genreID;
    }

    public void setGenreID(Integer genreID) {
        this.genreID = genreID;
    }

    public Long getPlaylistID() {
        return playlistID;
    }

    public void setPlaylistID(Long playlistID) {
        this.playlistID = playlistID;
    }
}
