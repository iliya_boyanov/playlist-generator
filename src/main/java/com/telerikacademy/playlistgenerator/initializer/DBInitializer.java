package com.telerikacademy.playlistgenerator.initializer;

import com.telerikacademy.playlistgenerator.dtos.GenreList;
import com.telerikacademy.playlistgenerator.dtos.PlaylistList;
import com.telerikacademy.playlistgenerator.dtos.TrackList;
import com.telerikacademy.playlistgenerator.models.*;
import com.telerikacademy.playlistgenerator.repositories.AlbumRepository;
import com.telerikacademy.playlistgenerator.repositories.ArtistRepository;
import com.telerikacademy.playlistgenerator.repositories.GenreRepository;
import com.telerikacademy.playlistgenerator.repositories.TrackRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class DBInitializer {

    private GenreRepository genreRepository;
    private TrackRepository trackRepository;
    private ArtistRepository artistRepository;
    private AlbumRepository albumRepository;
    private final List<String> genreNames = Arrays.asList("Rock", "Electro", "Rap/Hip Hop");
    private static final Logger LOGGER = LoggerFactory.getLogger(DBInitializer.class);
    @Autowired
    public DBInitializer(GenreRepository genreRepository, TrackRepository trackRepository, ArtistRepository artistRepository, AlbumRepository albumRepository) {
        this.genreRepository = genreRepository;
        this.trackRepository = trackRepository;
        this.artistRepository = artistRepository;
        this.albumRepository = albumRepository;
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public CommandLineRunner run(RestTemplate restTemplate, Environment environment) {
        return args -> {
            if (environment.getProperty("database.init").equalsIgnoreCase("true")) {
                if (genreRepository.findAll() == null || ((List<Genre>) genreRepository.findAll()).size() == 0) {
                    List<Genre> genreList = getAllGenres(restTemplate);
                    for (int i = 1; i < genreList.size(); i++) {
                        genreRepository.save(genreList.get(i));
                    }
                }

                for (String genreName : genreNames) {
                    List<Playlist> playlists = getAllPlaylistsByGenre(genreName, restTemplate);
                    List<Track> currentGenreTracks = getAllTracksFromPlaylists(playlists, restTemplate);
                    for (Track track : currentGenreTracks) {
                        track.setGenre(genreRepository.getGenreByName(genreName).orElse(null));
                        Artist currentArtist = artistRepository.getArtistByName(track.getArtist().getName());
                        Album currentAlbum = albumRepository.getAlbumByTitle(track.getAlbum().getTitle());
                        List<Track> tracksWithSameName = trackRepository.getTrackByTitle(track.getTitle());
                        if (tracksWithSameName == null || !tracksWithSameName.contains(track)) {
                            if (currentArtist != null) {
                                track.setArtist(currentArtist);
                            }
                            if (currentAlbum != null) {
                                track.setAlbum(currentAlbum);
                            }
                            trackRepository.save(track);
                        }
                    }

                    LOGGER.info("Tracks for genre " + genreName + " have been saved to the data base!");
                }
            }
        };
    }

    private List<Track> getAllTracksFromPlaylists(List<Playlist> playlists, RestTemplate restTemplate) throws InterruptedException {
        List<Track> tracks = new ArrayList<>();
        for (Playlist playlist : playlists) {
            String playlistTracksURL = "https://api.deezer.com/playlist/" + playlist.getId() + "/tracks";
            TrackList trackList = restTemplate.getForObject(playlistTracksURL, TrackList.class);
            tracks.addAll(trackList.getTrackList());
            while (trackList.getNext() != null) {
                Thread.sleep(100);
                playlistTracksURL = trackList.getNext();
                trackList = restTemplate.getForObject(playlistTracksURL, TrackList.class);
                tracks.addAll(trackList.getTrackList());
            }
            if(tracks.size() >= 1000){
                break;
            }
        }

        return tracks;
    }

    private List<Playlist> getAllPlaylistsByGenre(String genreName, RestTemplate restTemplate) {
        String findPlaylistsByGenreURL = "https://api.deezer.com/search/playlist?q=" + genreName;
        PlaylistList playlistList = restTemplate.getForObject(findPlaylistsByGenreURL, PlaylistList.class);
        return playlistList.getPlaylists();
    }

    private List<Genre> getAllGenres(RestTemplate restTemplate) {
        String allGenresUri = "https://api.deezer.com/genre";
        GenreList genreList = restTemplate.getForObject(allGenresUri, GenreList.class);
        return genreList.getGenreList();
    }

}
