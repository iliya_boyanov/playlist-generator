# [Playlist Generator](http://playlist-lb-701670138.eu-west-1.elb.amazonaws.com/?fbclid=IwAR3fO5VbmqP_49Vo_PhDzkzbrq6oFv_NzszrKAejG6z4UB7KiqX_UESbn6A)


Playlist Generator enables your users to generate playlists for specific travel duration periods based on their preferred genres

![Iliya & Deyvid](https://user-images.githubusercontent.com/20488473/55360824-29118e00-54de-11e9-9e9d-807f2ecd9905.png)


## 
  - Visible without authentication
    - the application start page
    - the user login form
    - the user registration form
    - list of all playlists and basic information about them


  - All playlists on the home page can be filtered by 
    - title
    - genre 
    - duration 
    
  - When authenticated, user can click on each playlist (via button), see its details, and can play preview of the tracks in it. Also 'Generate' button appears which allows user to generate new playlists.

  - The app have profile page that shows user’s first name, last name, email and a table with information about all playlists and buttons for details, update and delete
 
  - The admin page shows two tables - with users and with playlists supporting the CRUD operation and a button for Genre Synchronisation

### Technologies

Dillinger uses a number of open source projects to work properly:

- *IntelliJ IDEA* - as a IDE during the whole development process
- *Spring MVC* - as an application framework
- *Bootstrap 4* - for the front-end of the application making it responsive and providing ready to use stylesheets
- *Thymeleaf* - as a template engine in order to load a big amount of data (playlists, users). Also thymeleaf allows inserting and replacing views and imports, which makes the code way more readable and replaceable
- *MySQL (MariaDB)* - as database
- *Hibernate* - access the database
- *Spring Security* - for managing users, roles and authentication
- *Git* - for source control management and documentation / manual of the application


### Routes

| Mapping | Description |
| ------ | ------ |
| / | Home page with playlists listed and filter buttons|
| /generation| Generation page where you should fill the properties of two location via text boxes, set playlist title and pick up genres |
| /playlist-details/{id} | Playlist details page holding details of playlist with id of {id} |
| /update-playlist/{id} | Update playlist page where you can update the title and the genres of playlist. |
| /register | Register page with six must-fill text-boxes|
| /user/details | User page providing information about the user and CRUD operations over its playlists |
| /login | Login page providing authentication via username and password|
| /login?logout=true | Users are logged out and redirected to login page |
| /admin | Admin page where you can do the CRUD operation over playlists and users|
| /admin/create-user | Create user page which is being accessed from the admin panel|

### Test users

| Username | Password | Role |
| ------ | ------ | ------ |
| user | 12345 | user |
| iliya | 12345 | admin |

### Screenshots

- Home without authentication
![home-not-authenticated](https://user-images.githubusercontent.com/20488473/57427467-b4dcaf80-722c-11e9-890c-196a2d8b83cf.PNG)

- Home with authentication
![logged-home](https://user-images.githubusercontent.com/20488473/57427329-249e6a80-722c-11e9-84db-18ac9fbc3450.PNG)

- Generate playlist
![generate-playlist](https://user-images.githubusercontent.com/20488473/57427334-25cf9780-722c-11e9-851b-4628a2d21a0f.PNG)

- Playlist details
![playlist-details](https://user-images.githubusercontent.com/20488473/57427331-25370100-722c-11e9-8b54-60752ef1bdb2.PNG)

- User profile
![user-profile](https://user-images.githubusercontent.com/20488473/57427326-249e6a80-722c-11e9-9ed4-3f11e674cc9c.PNG)

- Admin
![admin-panel](https://user-images.githubusercontent.com/20488473/57427332-25370100-722c-11e9-87f3-6a4f0b7c5ba0.PNG)

- Update playlist
![update-playlist](https://user-images.githubusercontent.com/20488473/57427327-249e6a80-722c-11e9-9a98-6c8fcd3af51b.PNG)

- Update user
![update-user](https://user-images.githubusercontent.com/20488473/57427333-25370100-722c-11e9-9d55-278f11510ce4.PNG)

- Login
![login2](https://user-images.githubusercontent.com/20488473/57427324-249e6a80-722c-11e9-9428-7c1e251e4fa0.PNG)

- Register page
![register](https://user-images.githubusercontent.com/20488473/57427328-249e6a80-722c-11e9-871b-b1bfa99223b5.png)
